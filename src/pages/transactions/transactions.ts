import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Transaction } from '../../database';
import { AddingPage } from '../adding/adding';

/*
  Generated class for the Transactions page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html'
})
export class TransactionsPage {
	title : string = "Movimientos";
  transactions : any;
  Adding = AddingPage;

  constructor(public navCtrl: NavController) {}

  ionViewWillEnter() {
    //console.log('Hello TransactionsPage Page');
    this.loadTransactions();
    //let transaction = new Transaction(20, "Primer titulo");
    //transaction.save();
  }

  loadTransactions(){
    Transaction.all()
               .then((resultados) => { 
                 this.transactions = resultados;
                 //console.log(this.transactions);
               });
  }

}
