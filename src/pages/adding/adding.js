var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Transaction } from '../../database';
import { GeolocationService } from '../../services/geolocation.service';
/*
  Generated class for the Adding page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var AddingPage = (function () {
    function AddingPage(navCtrl, geolocator) {
        this.navCtrl = navCtrl;
        this.geolocator = geolocator;
        //title : string;
        //amount : number;
        this.shouldGeolocate = false;
        this.shouldSend = true;
        this.model = new Transaction(null, "");
    }
    AddingPage.prototype.ionViewDidLoad = function () {
        //this.amount = this.model.amount;
        //this.title = this.model.title;
        //console.log(this.model.amount);
    };
    AddingPage.prototype.save = function () {
        var _this = this;
        if (this.shouldSend) {
            // code...
            //Ejeguta un promesa
            this.model.save().then(function (result) {
                _this.model = new Transaction(null, "");
                _this.navCtrl.pop();
            });
        }
    };
    AddingPage.prototype.getLocation = function () {
        var _this = this;
        if (this.shouldGeolocate) {
            this.shouldSend = false;
            this.geolocator.get().then(function (resultado) {
                _this.model.setCoords(resultado.coords);
                console.log(_this.model);
                _this.shouldSend = true;
            }).catch(function (err) { return console.log(err); });
        }
        else {
            this.model.clearCoords();
        }
    };
    AddingPage = __decorate([
        Component({
            selector: 'page-adding',
            templateUrl: 'adding.html'
        }), 
        __metadata('design:paramtypes', [NavController, GeolocationService])
    ], AddingPage);
    return AddingPage;
}());
//# sourceMappingURL=adding.js.map