import Dexie from 'dexie';
//Configuracion de la base de datos
export class TransactionAppDB extends Dexie{
  transactions: Dexie.Table<ITransaction, number>;

    constructor() {
        super("MoneyMapAppDB");
        this.version(1).stores({
            transactions: "++id,amount,lat,lng,title,imageUrl"
        });
        this.transactions.mapToClass (Transaction);

    }

}


export interface ICategory{

}
//Se guarda el id(Identificador)
export interface ITransaction{
  //(Es opcional => ?)
  id? : number;
  amount: number;
  lat: number;
  lng: number;
  title: string;
  imageUrl: string;
}
//Este es nuestro modelo
export class Transaction implements ITransaction {
  id? : number;
  amount: number;
  lat: number;
  lng: number;
  title: string;
  imageUrl: string;

  constructor(amount:number, title:string, lat?:number,lng?:number,id?:number, imageUrl?:string) {
    this.amount = amount;
    this.title = title;
    if(lat) this.lat = lat;
    if(lng) this.lng = lng;
    if(imageUrl) this.imageUrl = imageUrl;
    if(id) this.id = id;
  }

  save(){
    return db.transactions.add(this);
  }

  setCoords(coords){
    this.lat = coords.latitude;
    this.lng = coords.longitude;
  }

  clearCoords(){
    this.lat = null;
    this.lng = null;
  }
  
  getImage():string{
    if(this.imageUrl)
      return this.imageUrl;
    return "blue";
  }

  hasLocation():boolean{
    return !!(this.lat && this.lng);
  }


  static all(){
    //Transaction.all => Todas las transaciones
    return db.transactions.orderBy("id").reverse().toArray();
    //Tipo promise
  }
}

export let db = new TransactionAppDB();
