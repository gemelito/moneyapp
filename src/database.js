var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import Dexie from 'dexie';
//Configuracion de la base de datos
export var TransactionAppDB = (function (_super) {
    __extends(TransactionAppDB, _super);
    function TransactionAppDB() {
        _super.call(this, "MoneyMapAppDB");
        this.version(1).stores({
            transactions: "++id,amount,lat,lng,title,imageUrl"
        });
        this.transactions.mapToClass(Transaction);
    }
    return TransactionAppDB;
}(Dexie));
//Este es nuestro modelo
export var Transaction = (function () {
    function Transaction(amount, title, lat, lng, id, imageUrl) {
        this.amount = amount;
        this.title = title;
        if (lat)
            this.lat = lat;
        if (lng)
            this.lng = lng;
        if (imageUrl)
            this.imageUrl = imageUrl;
        if (id)
            this.id = id;
    }
    Transaction.prototype.save = function () {
        return db.transactions.add(this);
    };
    Transaction.prototype.setCoords = function (coords) {
        this.lat = coords.latitude;
        this.lng = coords.longitude;
    };
    Transaction.prototype.clearCoords = function () {
        this.lat = null;
        this.lng = null;
    };
    Transaction.all = function () {
        //Transaction.all => Todas las transaciones
        return db.transactions.orderBy("id").reverse().toArray();
        //Tipo promise
    };
    return Transaction;
}());
export var db = new TransactionAppDB();
//# sourceMappingURL=database.js.map